---
sidebar: auto
---

# Meltano Press

Meltano related news, logos, and how to get in touch.

## Top Stories

- [GitLab to create tool for data teams](https://sdtimes.com/data/gitlab-to-create-tool-for-data-teams/)
- [Hey, data teams - We're working on a tool just for you](https://about.gitlab.com/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/)
- [Meltano Meeting Agenda](https://docs.google.com/document/d/1nayKquFLL8DN3h8mnLo3pVZsEKyPcBgQm2mqc5GggPA)
- [Hacker News discussion](https://news.ycombinator.com/item?id=17667399)
- [Call with Hacker News commenter](https://www.youtube.com/watch?v=F8tEDq3K_pE)

## Logos

<LogoList />

## Get in Touch

For press inquiries, please email [hello@meltano.com](mailto:hello@meltano.com).

You can also follow us on: 

- [YouTube](https://www.youtube.com/meltano)
- [Twitter](https://twitter.com/meltanodata)
- [Slack](https://join.slack.com/t/meltano/shared_invite/enQtNTM2NjEzNDY2MDgyLWI1N2EyZjA1N2FiNDBlNDE2OTg4YmI1N2I3OWVjOWI2MzIyYmJmMDQwMTY2MmUwZjNkMTBiYzhiZTI2M2IxMDc)
